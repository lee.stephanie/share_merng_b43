//Require express
const express = require("express")
const router = express.Router() // we use express' routing to create the routes


const mongoose = require("mongoose")

const Member = require("../models/Member.js")

// Actual routes



router.get("/", (req,res)=>{ 
	Member.find().then(members => { 

		res.send(members)

	})

})

router.post("/", (req,res)=>{ 

	// res.send(
	// 	"User " + 
	// 	req.body.lastName + 
	// 	", " + 
	// 	req.body.firstName + 
	// 	" applying for " + 
	// 	req.body.position + 
	// 	" position")

	let newMember = new Member({

		firstName : req.body.firstName,
		lastName  : req.body.lastName, 
		position  : req.body.position

	})

	newMember.save( (saveErr, newMember) => { 

		// if an error encountered while saving document output the error in the console.
		if(saveErr) return console.log(saveErr)
			//if successful, return a response saying "Member created"
		res.send("Member created")


	})


})


//Retrieve a single member

	// app.get("/members/:id", (req,res) => { 

	// 	// res.send("Looking for user with id = " + req.params.id)

	// 	// Retrieve a single member
	// 	// res.send("Looking for user with id = " + req.params.id)
	// 	Member.findById(req.params.id, (err, member) => {
	// 		if(err) return console.log(err)
	// 			res.send(member)
	// 	})



	// })



//Delete
router.delete("/:id", (req, res)=>{ 
	//fill this out 
	// if id is not found, return the statement "No user found" 
	// if id is found, delete the document and return "user deleted"
	//Q: How to delete using mongoose
	Member.findByIdAndDelete(req.params.id, (err) => {
		if(err) { 
			return res.send("User not found")
		}
			res.send("User Deleted")
	})


})


//Update

router.put("/:id", (req,res)=>{ 

	Member.findByIdAndUpdate(req.params.id, { 
	// the params determine which entry to update 
	// and the body determines what values to update
		$set:{

			firstName	: req.body.firstName, 
			lastName	: req.body.lastName, 
			position	: req.body.position 
		}

	}, (err) => { 

		if(err){
			return res.send("No user found")
		}
		res.send("User updated")

	})


})


module.exports = router
