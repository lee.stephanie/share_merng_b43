const express = require("express")
const router = express.Router() // we use express' routing to create the routes

const mongoose = require("mongoose")


const Item = require("../models/Item.js")

//Exercise 
// 1. Retrieve all teams
router.get("/",(req,res)=>{ 

	Item.find().then(items => { 
		res.send(items)

	})

}) 			//localhost:4000/teams
// router.get("/popoy")		//localhost:4000/teams/popoy



//2. Create a team 
router.post("/",(req,res)=>{ 

	let newItem = new Item({

		name : req.body.name,
		price  : req.body.price, 
		teamId  : req.body.teamId,
		quantity : req.body.quantity,


	})

	newItem .save( (saveErr, newItem) => { 

		// if an error encountered while saving document output the error in the console.
		if(saveErr) return console.log(saveErr)
			//if successful, return a response saying "Member created"
		res.send("Item Created")


	})

})



//3. Retrieve a single team 

router.get("/:id", (req,res)=>{ 

	Item.findById(req.params.id, ( err, item ) => { 
		if(err) return console.log(err)
		res.send(item)


	})

})

// 4. Delete a team

router.delete("/:id", (req, res)=>{ 

	Item.findByIdAndDelete(req.params.id, (err) => {
		if(err) { 
			return res.send("User not found")
		}
			res.send("User Deleted")
	})

}) 

module.exports = router
