const express = require("express")
const router = express.Router() // we use express' routing to create the routes

const mongoose = require("mongoose")


const Team = require("../models/Team.js")

//Exercise 
// 1. Retrieve all teams
router.get("/",(req,res)=>{ 

	Team.find().then(teams => { 
		res.send(teams)

	})

}) 			//localhost:4000/teams
// router.get("/popoy")		//localhost:4000/teams/popoy



//2. Create a team 
router.post("/",(req,res)=>{ 

	let newTeam = new Team({

		name : req.body.name

	})

	newTeam.save( (saveErr, newTeam) => { 

		// if an error encountered while saving document output the error in the console.
		if(saveErr) return console.log(saveErr)
			//if successful, return a response saying "Member created"
		res.send("Team Created")


	})

})



//3. Retrieve a single team 

router.get("/:id", (req,res)=>{ 

	Team.findById(req.params.id, ( err, teams ) => { 
		if(err) return console.log(err)
		res.send(teams)


	})

})

// 4. Delete a team

router.delete("/:id", (req, res)=>{ 

	Team.findByIdAndDelete(req.params.id, (err) => {
		if(err) { 
			return res.send("User not found")
		}
			res.send("User Deleted")
	})

}) 

module.exports = router
