const { ApolloServer, gql } = require("apollo-server-express");

const bcrypt = require("bcrypt");

//mongoose models

const Member = require("../models/Member");
const Task = require("../models/Task");
const Team = require("../models/Team");
const Item = require("../models/Item");

// CRUD
// type Query == Retrieve / Read
// type Mutation == Create / Update

const typeDefs = gql`
	# this is a comment
	# the type Query is the root of all GraphQL queries
	# this is used for executing "GET" requests
	# this is what you call a schema

	type TeamType {
		id: ID
		name: String
		tasks: [TaskType]
	}

	type TaskType {
		id: ID
		description: String!
		teamId: ID!
		isCompleted: Boolean!
		team: [TeamType]
	}

	type MemberType {
		id: ID
		firstName: String!
		lastName: String!
		position: String!
		teamId: String!
		team: [TeamType]
		password: String!
	}

	type ItemType {
		id: ID
		name: String!
		price: Float!
		teamId: String!
		quantity: Float!
		team: [TeamType]
	}

	type Query {
		# create a query called hello that will expect a string data type.
		hello: String!

		getTeams: [TeamType]
		getTasks: [TaskType]
		getMembers: [MemberType]
		getItems: [ItemType]

		# (id: ID!) -> is an example of an args / arguement

		getMember(id: ID!): MemberType

		getTeam(id: ID!): TeamType

		getTask(id: ID!): TaskType

		getItem(id: ID!): ItemType

		#to make it required just add !
	}

	#CUD functionality
	# we are mutating the server / database

	type Mutation {
		createTeam(name: String!): TeamType

		# this means that we can insert name on the Team database.

		createTask(
			description: String!
			teamId: String!
			isCompleted: Boolean
		): TaskType

		# this means that we can insert name on the Task in the database

		createMember(
			firstName: String!
			lastName: String!
			position: String!
			teamId: String!
			password: String!
		): MemberType

		createItem(
			name: String!
			price: Float!
			teamId: String!
			quantity: Float!
		): ItemType

		updateTeam(id: ID!, name: String!): TeamType

		updateTask(
			id: ID!
			description: String
			teamID: ID
			isCompleted: Boolean
		): TaskType

		updateMember(
			id: ID!
			firstName: String!
			lastName: String!
			position: String!
			teamId: String!
			password: String!
		): MemberType

		updateItem(
			id: ID!
			name: String!
			price: Float!
			teamId: String!
			quantity: Float!
		): ItemType

		deleteMember(id: String): Boolean
	}
`;

const resolvers = {
	Query: {
		hello: () => "my first query",

		getTeams: () => {
			return Team.find({});
		},

		getTasks: () => {
			return Task.find({});
		},

		getMembers: () => {
			return Member.find({});
		},

		getItems: () => {
			return Item.find({});
		},

		getMember: (parent, args) => {
			console.log("nagexecute ka ng getMember Query");
			console.log(args.id);
			// console.log(Member.findById(args.id))
			return Member.findById(args.id);
		},

		getTeam: (_, args) => {
			console.log(args);
			console.log("Baby nag getTeam Query ka");

			// return Team.findById(args.id)
			return Team.findOne({ _id: args.id });
		},

		getTask: (_, args) => {
			console.log(args);
			console.log("ui may task na siya");

			// return Task.findById(args.id)

			return Task.findOne({ _id: args.id });
		},

		getItem: (_, args) => {
			console.log(args);
			console.log("kumukuha ng gamit");

			return getItem.findOne({ _id: args.id });
		}
	},

	Mutation: {
		createTeam: (_, args) => {
			console.log(args);

			let newTeam = Team({
				name: args.name
			});

			console.log(newTeam);
			return newTeam.save();
		},

		createTask: (_, args) => {
			console.log(args);

			let newTask = Task({
				description: args.description,
				teamId: args.teamId,
				isCompleted: args.isCompleted
			});

			return newTask.save();
		},

		createMember: (_, args) => {
			console.log(args);

			let newMember = Member({
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamId: args.teamId,
				//bcrypt.hashSync(plain_password, # of salt rounds)
				password: bcrypt.hashSync(args.password, 10)
			});
			console.log(newMember);
			return newMember.save();
		},

		createItem: (_, args) => {
			console.log(args);

			let newItem = Item({
				name: args.name,
				price: args.price,
				teamId: args.teamId,
				quantity: args.quantity
			});

			return newItem.save();
		},

		updateTeam: (_, args) => {
			console.log("Updating team info...");
			console.log(args);
			// Team.findOneAndUpdate(condition, update, callbackfunction)
			// Team.findOneAndUpdate(
			// { _id: args.id},
			// {
			// $set: {
			// name: args.name
			// }
			// },
			// (err)=> {
			// if(err) {
			// return console.log(err)
			// }
			// console.log("Team updated!")
			// }
			// )
			// return Team.findById(args.id)

			let condition = { _id: args.id };
			let updates = { name: args.name };

			return Team.findOneAndUpdate(condition, updates);
		},

		// updateTask : (_, args)=> {
		// 	console.log("Updating task info...");
		// 	console.log(args)
		// 				// Task.findOneAndUpdate(
		// 				// { _id: args.id},
		// 				// {
		// 				// $set: {
		// 				// description: args.description,
		// 				// teamID: args.teamID,
		// 				// isCompleted: args.isCompleted
		// 				// }
		// 				// },
		// 				// (err)=> {
		// 				// if(err) {
		// 				// return console.log(err)
		// 				// }
		// 				// console.log("Task updated!")

		// 				// }
		// 				// )

		// 	let condition = { _id: args.id}
		// 	let updates = {isCompleted: true}

		// 	return Task.findOneAndUpdate(condition,updates)
		// },

		// updateMember : (_, args)=> {
		// 	console.log("Updating member info...");
		// 	console.log(args)
		// 	Member.findOneAndUpdate(
		// 		{ _id: args.id},
		// 		{
		// 			$set: {
		// 				firstName: args.firstName,
		// 				lastName: args.lastName,
		// 				position: args.position
		// 			}
		// 		}
		// 		)
		// 	return Member.findById(args.id)
		// }

		updateTask: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = { isCompleted: true };

			return Task.findOneAndUpdate(condition, update);
		},

		updateMember: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamId: args.teamId,
				password: args.password
			};

			return Member.findOneAndUpdate(condition, update);
		},

		updateItem: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				name: args.name,
				price: args.price,
				teamId: args.teamId,
				quantity: args.quantity
			};

			return Item.findOneAndUpdate(condition, update);
		},

		deleteMember: (_, args) => {
			console.log(args.id);

			let condition = args.id;

			//first parameter (what is the condition? , callBack Function)
			return Member.findByIdAndDelete(condition, (err, member) => {
				console.log(err);
				console.log(member);

				if (err || !member) {
					console.log("delete failed. no user found");
					return false;
				}
				console.log("user deleted");
			});
		}
	},

	TeamType: {
		tasks: (parent, args) => {
			console.log("getting the tasks assigned for this team");
			console.log(parent.id);
			return Task.find({ teamId: parent.id });
		}
	},

	MemberType: {
		team: (parent, args) => {
			console.log("retrieving team details");
			console.log(parent);

			return Team.find({ _id: parent.teamId });
		}
	},

	TaskType: {
		team: (parent, args) => {
			console.log("retrieving team details");
			console.log(parent);

			return Team.find({ _id: parent.teamId });
		}
	},

	ItemType: {
		team: (parent, args) => {
			console.log("retrieving team details for item");
			console.log(parent);

			//this is the indicator that it is finding a value in the "teams" table
			// which is the database name where our values are.
			return Team.find({ _id: parent.teamId });
		}
	}

	// TaskType: {

	// 	team : (parents,args)=> {

	// 	console.log ("getting the tasks assigned for this team")
	// 	console.log(parent.id)

	// 		return Team.find({teamId : parent.id})

	// 	}

	// }
};

//create an instance of teh apollo server
// in the most basic sense, the ApolloServer can be started
// by passing schema type definitions (typeDefs) and the resolvers responsible for fetching the data for the
// requests/queries

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
