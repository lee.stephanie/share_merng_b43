// backend server
// Declaring dependencies


// use/import the express library
const express = require('express');

// instantiate an express project
// serve our project using express
const app = express();

// use the mongoose library
const mongoose = require("mongoose");

// database connection
mongoose.connect(
"mongodb+srv://admin:24Lj878aDUYQ06P3@nosqlsession-gdzha.mongodb.net/b43_merng_db?retryWrites=true&w=majority",
{
	useNewUrlParser : true
}
)

mongoose.connection.once("open", () => {
console.log("now connected to the online MongoDB server")
})


//import the instantiation of the apollo server

const server = require("./queries/queries.js")

// the app will be served by the apollo server instead of express.

server.applyMiddleware({ 

	app

})

// server initialization
app.listen(4000, ()=> { 

	console.log(`🚀  Server ready at http://localhost:4000${server.graphqlPath}`)

})

