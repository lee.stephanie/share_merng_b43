const mongoose = require("mongoose")
const Schema = mongoose.Schema // use mongoose Schema as our basis.

//Actual Schema

const taskSchema = new Schema ({ 

	description: String, 
	teamId: String, 
	isCompleted: Boolean 

	},
	{

		timestamps: true


})


// export the model as a module


module.exports = mongoose.model("Task", taskSchema);


//this creates a model called "Task" using the schema "taskSchema" 
//and exports it to be used by index.js