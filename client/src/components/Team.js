import React, { useState, useEffect } from "react";
import { Container, Columns, Card, Tag, Button } from "react-bulma-components";
//graphQL
import { graphql } from "react-apollo";
//import the query
import { getTeamsQuery } from "../queries/queries.js";
import Swal from "sweetalert2";

//composed lodash
import { flowRight as composed } from "lodash";

//import mutation

import { createTeamMutation } from "../queries/mutation";

const Team = props => {
	useEffect(() => {
		console.log("team: " + name);
	});
	/*console.log(props.getTeamsQuery);*/
	/*const data = props.data;*/

	const teamData = props.getTeamsQuery.getTeams
		? props.getTeamsQuery.getTeams
		: [];

	if (teamData.loading) {
		let timerInterval;
		Swal.fire({
			title: "Auto close alert!",
			html: "I will close in <b></b> milliseconds.",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	const [name, setName] = useState("");

	const teamChangeHandler = e => {
		console.log(e.target.value);
		setName(e.target.value);
	};

	// console.log(teamData);

	//add team

	const addTeam = e => {
		e.preventDefault();

		let newTeam = {
			name: name
		};

		console.log(newTeam);
		/*console.log(props);*/

		props.createTeamMutation({
			variables: newTeam,
			refetchQueries: [
				{
					query: getTeamsQuery
				}
			]
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Team Management
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<Tag color="warning">Join us. Be a Hero.</Tag>
							<form onSubmit={addTeam}>
								<div className="field">
									<label className="label" htmlFor="name">
										Add A Team
									</label>
									<input
										id="name"
										className="input"
										type="text"
										onChange={teamChangeHandler}
										value={name}
									/>
								</div>

								<Button type="submit" color="success">
									Add New Team
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Side Kick Membership List
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<th className="has-text-centered">
											Team Name
										</th>

										<th className="has-text-centered">
											Action
										</th>
									</thead>
									<tbody>
										{teamData.map(team => {
											return (
												<tr>
													<td>{team.name}</td>
													<td>
														<Button
															color="link"
															fullwidth
														>
															Update
														</Button>
														<Button
															color="danger"
															fullwidth
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

/*export default graphql(getTeamsQuery)(Team);*/

export default composed(
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createTeamMutation, { name: "createTeamMutation" })
)(Team);

/*export default composed(
	graphql(getMembersQuery, { name: "getMembersQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createMemberMutation, { name: "createMemberMutation" })
)(Member);
*/
