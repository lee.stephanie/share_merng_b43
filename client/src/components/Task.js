// whenever we are using JSX, we need to import React
import React, { useState } from "react";
import { Container, Columns, Card, Tag, Button } from "react-bulma-components";
import { flowRight as composed } from "lodash";

//graphQL
import { graphql } from "react-apollo";

//import the query
import { getTasksQuery, getTeamsQuery } from "../queries/queries.js";
import Swal from "sweetalert2";

//import mutation

import { createTaskMutation } from "../queries/mutation";

const Task = props => {
	/*	console.log(props); */ //undefined
	/*useEffect(() => {
		console.log("description" + description);
		console.log("firstname: " + firstName);
		console.log("position: " + position);
	});*/

	//store to the data variable the binded gql queries.
	/*const data = props.data; */
	/*console.log(data.getMembers);*/

	const taskData = props.getTasksQuery.getTasks
		? props.getTasksQuery.getTasks
		: [];

	const teamData = props.getTeamsQuery.getTeams
		? props.getTeamsQuery.getTeams
		: [];

	const [Task, setTask] = useState("");

	const TaskChangeHandler = e => {
		console.log(e.target.value);
		setTask(e.target.value);
	};

	const [team, setTeam] = useState("");

	const teamChangeHandler = e => {
		console.log(e.target.value);
		setTeam(e.target.value);
	};

	const addTask = e => {
		e.preventDefault();

		let newTask = {
			description: Task,
			teamId: team
		};
		/*
		console.log(props);*/

		props.createTaskMutation({
			variables: newTask,
			refetchQueries: [
				{
					query: getTasksQuery
				}
			]
		});
		/*
		console.log(newTask);*/
	};

	/*	const teamData = data.getTeams ? data.getTeams : [];
	console.log(teamData)*/
	// if(data.loading){
	// let timerInterval
	// Swal.fire({
	//   title: 'Auto close alert!',
	//   html: 'I will close in <b></b> milliseconds.',
	//   timer: 1000,
	//   timerProgressBar: true,
	//   onBeforeOpen: () => {
	//     Swal.showLoading()

	//   },
	//   onClose: () => {
	//     clearInterval(timerInterval)
	//   }
	// }).then((result) => {
	//   if (
	//     /* Read more about handling dismissals below */
	//     result.dismiss === Swal.DismissReason.timer
	//   ) {
	//     console.log('I was closed by the timer') // eslint-disable-line
	//   }
	// })

	// }

	/*	console.log(data)
	 */

	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Task Management
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<Tag color="warning">Join us. Be a Hero.</Tag>
							<form onChange={addTask}>
								<div className="field">
									<label className="label" htmlFor="task">
										Add a Task
									</label>
									<input
										id="task"
										className="input"
										type="text"
										onChange={TaskChangeHandler}
										value={Task}
									/>
								</div>

								{/*<div className="field">
									<label className="label" htmlFor="teamName">
										Status
									</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select
												onChange={teamChangeHandler}
											>
												<option disabled selected>
													Select Task
												</option>
												{taskData.map(task => {
													return (
														<option
															key={task.id}
															value={task.id}
														>
															{task.isCompleted.toString()}
														</option>
													);
												})}
											</select>
										</div>
									</div>
								</div>*/}

								<div className="field">
									<label className="label" htmlFor="status">
										Team
									</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select
												onChange={teamChangeHandler}
											>
												<option disabled selected>
													Select Team
												</option>
												{teamData.map(team => {
													return (
														<option
															key={team.id}
															value={team.id}
														>
															{team.name}
														</option>
													);
												})}
											</select>
										</div>
									</div>
								</div>
								<Button type="submit" color="success">
									Add New Task
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Side Kick Membership List
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<th className="has-text-centered">
											Description
										</th>
										<th className="has-text-centered">
											Teams
										</th>

										<th className="has-text-centered">
											Status
										</th>

										<th className="has-text-centered">
											Actions
										</th>
									</thead>
									<tbody>
										{taskData.map(task => {
											console.log(task.isCompleted);
											/*			console.log(task.teamId.name);*/

											return (
												<tr key={task.id}>
													<td>{task.description}</td>
													<td>{task.teamId}</td>
													<td>
														{task.isCompleted.toString()}
													</td>
													<td>
														<Button
															color="link"
															fullwidth
														>
															Update
														</Button>
														<Button
															color="danger"
															fullwidth
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default composed(
	graphql(getTasksQuery, { name: "getTasksQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createTaskMutation, { name: "createTaskMutation" })
)(Task);

// title = code
// description = eat-sleep-code
