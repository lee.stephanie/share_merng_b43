// whenever we're using JSX, we need to import React
import React, { useState } from "react";

// components
import Counter from "./Counter";

const Hooks = props => {
	// [current value, sets/assigns the new value] = useState(initial value)
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	console.log(props);

	// <Form />

	// react hooks cannot be nested inside if-else conditions
	// if (true) {
	// 	const [count, setCount] = useState(5000);
	// }

	const emailInputHandler = e => {
		// console.log("you clicked the input handler");
		console.log(e.target.value);
		setEmail(e.target.value);
	};

	const passwordInputHandler = e => {
		console.log(e.target.value);
		setPassword(e.target.value);
	};

	return (
		<div>
			<Counter initialCount={25} />
			<Counter initialCount={55} />
			<Counter initialCount={55555555} />
			<p>Hello from the Hooks Component</p>
			<p>Hello, {email}</p>
			<input onChange={emailInputHandler} value={email} />
			<p>Your password is: {password} </p>
			<input
				type="password"
				onChange={passwordInputHandler}
				className="form-control"
				value={password}
			/>
		</div>
	);
};

// export the Hooks component
export default Hooks;
