// whenever we are using JSX, we need to import React
import React, { useState, useEffect } from "react";
import { Container, Columns, Card, Tag, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as composed } from "lodash";
import { Link } from "react-router-dom";
//import the query
import { getMembersQuery, getTeamsQuery } from "../queries/queries.js";
import Swal from "sweetalert2";

//import Mutation

import {
	createMemberMutation,
	deleteMemberMutation
} from "../queries/mutation";

const Member = props => {
	//hooks

	//use effect
	useEffect(() => {
		console.log("lastname:" + lastName);
		console.log("firstname: " + firstName);
		console.log("position: " + position);
		console.log("team: " + team);
		console.log("password: " + password);
	});
	// password
	const [password, setPassword] = useState("");

	const passwordChangeHandler = e => {
		console.log(e.target.value);
		setPassword(e.target.value);
		console.log(password);
	};

	//firstName
	const [firstName, setFirstName] = useState("");

	const firstNameChangeHandler = e => {
		console.log(e.target.value);
		setFirstName(e.target.value);
		console.log(firstName);
	};

	//lastName
	const [lastName, setlastName] = useState("");

	const lastNameChangeHandler = e => {
		console.log(e.target.value);
		setlastName(e.target.value);
	};

	//position
	const [position, setPosition] = useState("");

	const positionChangeHandler = e => {
		console.log(e.target.value);
		setPosition(e.target.value);
	};

	//team

	const [team, setTeam] = useState("");

	const teamChangeHandler = e => {
		console.log(e.target.value);
		setTeam(e.target.value);
	};

	const addMember = e => {
		e.preventDefault();

		//register to system
		let newMember = {
			firstName: firstName,
			lastName: lastName,
			position: position,
			teamId: team,
			password: password
		};

		console.log(newMember);
		//swal

		props.createMemberMutation({
			variables: newMember,
			refetchQueries: [
				{
					query: getMembersQuery
				}
			]
		});

		if (!newMember.value) {
			Swal.fire({
				icon: "error",
				title: "Please fill up the form",
				text: "Something went wrong!",
				footer: "<a href>Why do I have this issue?</a>"
			});
		} else {
			Swal.fire({
				position: "top-end",
				icon: "success",
				title: "Successfully added new member",
				showConfirmButton: false,
				timer: 1500
			});
		}
		//clear input boxes

		setFirstName("");
		setlastName("");
		setPosition("");
	};

	/*	console.log(props.getMembersQuery.getMembers);
	 */

	/*console.log(props);*/

	//store to the data variable the binded gql queries.
	/*	const data = props.data; */
	/*console.log(data.getMembers);*/

	const memberData = props.getMembersQuery.getMembers
		? props.getMembersQuery.getMembers
		: [];
	/*console.log(memberData)

	

*/
	/*
console.log(props.getTeamsQuery.getTeams)*/
	const teamData = props.getTeamsQuery.getTeams
		? props.getTeamsQuery.getTeams
		: [];
	/*	console.log(teamData[0]);*/

	/*	console.log(data)*/

	/*	let loadingMessage = data.loading ? (

		 <button className="control is-loading">fetching members..</button> ) : ( 

		 "");*/

	/*		 if(data.loading){ 
			let timerInterval
			Swal.fire({
			  title: 'Auto close alert!',
			  html: 'I will close in <b></b> milliseconds.',
			  timer: 1000,
			  timerProgressBar: true,
			  onBeforeOpen: () => {
			    Swal.showLoading()

			  },
			  onClose: () => {
			    clearInterval(timerInterval)
			  }
			}).then((result) => {
			  if (
			   
			    result.dismiss === Swal.DismissReason.timer
			  ) {
			    console.log('I was closed by the timer') // eslint-disable-line
			  }
			})

		 }
*/

	const [check, setCheck] = useState(false);

	const checkHandler = e => {
		console.log(e.target);
		setCheck(!check);
	};

	const deleteMemberHandler = e => {
		console.log("deleting a member...");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteMemberMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getMembersQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The member has been deleted.",
					"success"
				);
			}
		});
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Member Management
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<Tag color="warning">Join us. Be a Hero.</Tag>
							<form onSubmit={addMember}>
								<div className="field">
									<label className="label" htmlFor="fname">
										First Name
									</label>
									<input
										id="fname"
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										value={firstName}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="lname">
										Last Name
									</label>
									<input
										id="lname"
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										value={lastName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="position">
										Position
									</label>
									<input
										id="position"
										className="input"
										type="text"
										onChange={positionChangeHandler}
										value={position}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="text"
										onChange={passwordChangeHandler}
										value={password}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="teamName">
										Team
									</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select
												onChange={teamChangeHandler}
											>
												<option disabled selected>
													Select Team
												</option>

												{teamData.map(team => {
													return (
														<option
															key={team.id}
															value={team.id}
														>
															{team.name}
														</option>
													);
												})}
											</select>
										</div>
									</div>
								</div>
								<Button type="submit" color="success">
									Add New Member
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Side Kick Membership List
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									{/*									{loadingMessage}
									 */}{" "}
									<thead>
										<tr>
											<th className="has-text-centered">
												First Name
											</th>
											<th className="has-text-centered">
												Last Name
											</th>
											<th className="has-text-centered">
												Position Name
											</th>

											<th className="has-text-centered">
												Action
											</th>
										</tr>
									</thead>
									<tbody>
										{memberData.map(member => {
											/*												console.log(member.team[0].name)
											 */
											let team = member.team[0];

											return (
												<tr key={member.id}>
													<td>{member.firstName}</td>
													<td>{member.lastName}</td>
													<td>{member.position}</td>
													<td>{team.name}</td>
													<td>
														<Link
															to={
																"/member/update/" +
																member.id
															}
														>
															<Button
																color="link"
																fullwidth
															>
																Update
															</Button>
														</Link>

														<Button
															id={member.id}
															onClick={
																deleteMemberHandler
															}
															color="danger"
															fullwidth
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

/*export default graphql(getMembersQuery)(Member);
 */

export default composed(
	graphql(getMembersQuery, { name: "getMembersQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createMemberMutation, { name: "createMemberMutation" }),
	graphql(deleteMemberMutation, { name: "deleteMemberMutation" })
)(Member);
