import React, { useState, useEffect } from "react";

const Counter = props => {
	const [count, setCount] = useState(props.initialCount);

	console.log(props);

	const incrementCountHandler = () => {
		console.log("you clicked the increment button");
		setCount(currentCount => currentCount + 1);
	};

	const decrementCountHandler = () => {
		setCount(currentCount => currentCount - 1);
	};

	return (
		<div>
			<p>hello from the Counter component</p>
			<p>display current count: {count}</p>
			<button onClick={incrementCountHandler}>increment</button>
			<button onClick={decrementCountHandler}>decrement</button>
		</div>
	);
};

export default Counter;
