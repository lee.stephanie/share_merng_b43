import React from "react";

const Image = props => {
	console.log(props);

	return (
		<div>
			<p>hello from the Image component</p>
			<img src={props.img} />
		</div>
	);
};

export default Image;
