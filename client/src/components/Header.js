// whenever we are using JSX, we need to import React
import React from "react";
import logo from "./../logo.svg";

const Header = props => {
	console.log(props);
	return (
		<header className="App-header">
			<img src={logo} className="App-logo" alt="logo" />
			{/* this is a comment*/}
			<p>{props.title}</p>
			<p> {props.intProp} </p>
			<p> {props.myArr[0]}</p>
			<p> {props.myArr[1]}</p>
			<p> {props.myArr[2]}</p>
			<p> {props.myObj.a} </p>
			<p> {props.myObj.b} </p>
			<p> {JSON.stringify(props.myObj)} </p>
			<p>{props.myFunc(10, 5)}</p>

			<a
				className="App-link"
				href="https://reactjs.org"
				target="_blank"
				rel="noopener noreferrer"
			>
				Wohoo React
			</a>
		</header>
	);
};

// export the component
export default Header;
