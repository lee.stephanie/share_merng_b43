import React, {useState} from "react";
import {Navbar} from "react-bulma-components"
import {Link} from "react-router-dom"


const NavBar = props => { 
	


	const [open,setOpen] = useState(false)

	let customLink = "";

		if(!props.username) { 
			customLink = <Link className="navbar-item">Guest</Link>
		} else { 

			customLink=<Link className="navbar-item"> {props.username} </Link>

		}



return ( 

		<Navbar color="black">
		<Navbar.Brand>
			<Link to="/" className="navbar-item">
				<strong>Batch 43</strong>


			</Link>
			<Navbar.Burger className="has-text-warning" active={open} onClick={()=>{

				setOpen(!open)

			}} />

		</Navbar.Brand>

		<Navbar.Menu>
			<Navbar.Container position="end">
				<Link className="navbar-item" to="/">
					Members
				</Link>
				<Link className="navbar-item" to="/teams">
					Teams
				</Link>

				<Link className="navbar-item" to="/tasks">
					Tasks
				</Link>
				<Link className="navbar-item" to="/">
					Hi, {customLink}
				</Link>
			</Navbar.Container>

		</Navbar.Menu>

		</Navbar>


	)





}

export default NavBar