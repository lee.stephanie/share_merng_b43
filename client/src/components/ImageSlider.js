import React, { useState, useEffect } from "react";
import { Button, Box } from "react-bulma-components";

// task
// - create an image slider/carousel using react hooks

const ImageSlider = props => {
	const images = [
		"/images/sbLogo.png",
		"/images/dog.jpg",
		"/images/1364.jpg"
	];

	useEffect(() => {
		console.log(`this is the value of index: ${index}`);
	});

	const [index, setIndex] = useState(0);

	console.log("the array length of images is: " + images.length);

	const nextImageHandler = () => {
		console.log("selecting next image...");
		// setIndex(index + 1);

		// ternary operator
		// condition ? true : false
		// condition ? if : else
		/*
			if(index === images.length - 1) {
				true
				setIndex(0)
			} else {
				false
				setIndex(index+1)
			}
		*/
		// console.log(index);
		index === images.length - 1 ? setIndex(0) : setIndex(index + 1);
	};

	const previousImageHandler = () => {
		console.log("selecting previous image...");
		// console.log(index);
		// setIndex(index - 1);
		index <= 0 ? setIndex(images.length - 1) : setIndex(index - 1);
	};
	// const timer = () => {
	// 	console.log("selecting previous image...");
	// 	setIndex(index + 1);
	// };

	// setInterval(timer, 2000);

	// console.log(index);

	return (
		<div>
			<h2>hello from the ImageSlider component </h2>
			<img
				style={{
					width: "100px",
					height: "100px"
				}}
				src={images[index]}
				alt="react carousel"
			/>
			<Box>
				<Button
					onClick={previousImageHandler}
					color="dark"
					outlined={true}
				>
					Previous
				</Button>
				<Button
					onClick={nextImageHandler}
					color="primary"
					outlined={false}
				>
					Next
				</Button>
			</Box>
		</div>
	);
};

export default ImageSlider;
