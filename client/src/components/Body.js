import React from "react";

const Body = props => {
	return <p> Hi! </p>;
};

const Body2 = () => {
	return (
		<div>
			<h1>this is from body2</h1>
			<h2>this is also from body2</h2>
		</div>
	);
};

// if you have multiple components to export, you need to explicitly
// define each component
export { Body, Body2 };
