import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container
} from "react-bulma-components";
import { flowRight as composed } from "lodash";
import { graphql } from "react-apollo";

// queries
import { getTeamsQuery, getMemberQuery } from "../queries/queries.js";

// mutations
import { updateMemberMutation } from "../queries/mutation.js";

const UpdateMember = props => {
	console.log(props);
	/*
	let member = props.getMember*/
	let member = props.getMemberQuery.getMember
		? props.getMemberQuery.getMember
		: {};
	/*	console.log(member);*/

	useEffect(() => {
		console.log("firstname: " + firstName);
		console.log("lastname: " + lastName);
		console.log("position: " + position);
		console.log("teamid: " + teamId);
	});

	// hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [position, setPosition] = useState("");
	const [teamId, setTeamId] = useState("");

	if (!props.getMemberQuery.loading) {
		const setDefaultValues = () => {
			console.log(member.teamId);
			setTeamId(member.teamId);
			setFirstName(member.firstName);
			setLastName(member.lastName);
			setPosition(member.position);
		};

		if (teamId === "") {
			setDefaultValues();
			console.log("teamId value after setDefault: " + teamId);
		}
	}

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
	};

	const positionChangeHandler = e => {
		setPosition(e.target.value);
	};

	const teamChangeHandler = e => {
		setTeamId(e.target.value);
	};

	const teamOptions = () => {
		// let teamData
		/*		console.log(props);*/
		let teamData = props.getTeamsQuery;
		if (teamData.loading) {
			return <option>Loading Teams...</option>;
		} else {
			return teamData.getTeams.map(team => {
				/*console.log(team);*/
				return (
					<option
						key={team.id}
						value={team.id}
						selected={team.id === teamId ? true : false}
					>
						{team.name}
					</option>
				);
			});
		}
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatedMember = {
			id: props.match.params.id,
			firstname: firstName,
			lastname: lastName,
			position: position,
			teamId: teamId
		};
		/*	console.log(updatedMember);*/

		props.updateMemberMutation({
			variables: updatedMember
		});
	};

	console.log(props.match);

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Heading>Update Member</Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Member Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<input
									type="text"
									onChange={firstNameChangeHandler}
									value={firstName}
									className="input"
								/>
								<input
									type="text"
									onChange={lastNameChangeHandler}
									value={lastName}
									className="input"
								/>
								<input
									type="text"
									onChange={positionChangeHandler}
									value={position}
									className="input"
								/>

								<div className="select is-fullwidth">
									<select onChange={teamChangeHandler}>
										{teamOptions()}
									</select>
								</div>

								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Update Member
								</button>

								<Link to="/">
									<button
										type="button"
										className="button is-danger is-fullwidth"
									>
										Cancel
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default composed(
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(updateMemberMutation, { name: "updateMemberMutation" }),
	graphql(getMemberQuery, {
		options: props => {
			// retrieve the wildcard id param
			/*		console.log(props.match.params.id);*/
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getMemberQuery"
	})
)(UpdateMember);
