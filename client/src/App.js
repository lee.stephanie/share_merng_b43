import React, { useState } from "react";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Box } from "react-bulma-components";
import { BrowserRouter, Route, Switch } from "react-router-dom";

//apollo

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

// components
import Header from "./components/Header";
import { Body, Body2 } from "./components/Body";
import Member from "./components/Member";
import Task from "./components/Task";
import Team from "./components/Team.js";
import Image from "./components/Image";
import Hooks from "./components/Hooks.js";
import ImageSlider from "./components/ImageSlider";
import NavBar from "./components/NavBar.js";
import UpdateMember from "./components/UpdateMember.js";
//create an instance to all our GraphQL components

const client = new ApolloClient({ uri: "http://localhost:4000/graphql" });

function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavBar username="Unggoy ka" />

        <Switch>
          <Route exact path="/" component={Member} />
          <Route path="/tasks" component={Task} />
          <Route path="/teams" component={Team} />
          <Route path="/member/update/:id" component={UpdateMember} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
