import { gql } from "apollo-boost";

const createMemberMutation = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $position: String!
    $teamId: String!
    $password: String!
  ) {
    createMember(
      firstName: $firstName
      lastName: $lastName
      position: $position
      teamId: $teamId
      password: $password
    ) {
      id
      firstName
      lastName
      position
      teamId
      password
    }
  }
`;

const createTeamMutation = gql`
  mutation($name: String!) {
    createTeam(name: $name) {
      name
    }
  }
`;

const createTaskMutation = gql`
  mutation($description: String!, $teamId: String!, $status: Boolean) {
    createTask(
      description: $description
      teamId: $teamId
      isCompleted: $status
    ) {
      description
      teamId
      isCompleted
    }
  }
`;

const deleteMemberMutation = gql`
  mutation($id: String!) {
    deleteMember(id: $id)
  }
`;

const updateMemberMutation = gql`
  mutation(
    $id: ID!
    $firstname: String!
    $lastname: String!
    $position: String!
    $teamId: String!
  ) {
    updateMember(
      id: $id
      firstName: $firstname
      lastName: $lastname
      position: $position
      teamId: $teamId
    ) {
      id
      firstName
      lastName
      position
      teamId
    }
  }
`;

const updateTeamMutation = gql`
  mutation($id: ID!, $name: String!) {
    updateTeam(id: $id, name: $name) {
      id
      name
    }
  }
`;

export {
  createMemberMutation,
  createTeamMutation,
  createTaskMutation,
  updateMemberMutation,
  deleteMemberMutation
};
