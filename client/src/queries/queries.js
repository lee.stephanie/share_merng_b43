import { gql } from "apollo-boost";

const getMembersQuery = gql`
  {
    getMembers {
      id
      firstName
      lastName
      position
      teamId
      team {
        name
        id
        tasks {
          description
        }
      }
    }
  }
`;

const getTasksQuery = gql`
  {
    getTasks {
      id
      description
      isCompleted
      teamId
    }
  }
`;

const getTeamsQuery = gql`
  {
    getTeams {
      id
      name
    }
  }
`;

const getTeamQuery = gql`
  query($id: ID!) {
    getTeam(id: $id) {
      id
      name
    }
  }
`;

const getMemberQuery = gql`
  query($id: ID!) {
    getMember(id: $id) {
      id
      firstName
      lastName
      position
      teamId
      team {
        name
      }
    }
  }
`;

export {
  getMembersQuery,
  getTasksQuery,
  getTeamsQuery,
  getMemberQuery,
  getTeamQuery
};
