what is react

-   js library for building user interfaces. since its js, it runs in the browser instead of the server

react is all about components
we can split a page into different components

-   header component
-   article component
-   sidebar component

why is this important?

-   if we split our page into smaller components, we can built it into contained pcs of code. we dont need to build the entire page, hence, more beneficial when working in teams
-   if we have a big app, instead of looking for the line where we built a sidenav for a blog post, we can just look for the sidenav component and edit it.
-   goal is to write maintainable, managable, reusable pcs of code that we can use in our page

auto indentation on save
npm i prettier

states/hooks

-   allows you to store data and easily change its value
